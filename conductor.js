var remote;
var apiKey;
var requestCallbacks = {};
var responseStatus = {
    WAITING: 'WAITING',
    SUCCESS: 'SUCCESS',
    ERROR: 'ERROR'
};
var Collection = {
    DEVICES: 'devices',
    SET_PROPERTY_VALUE_REQUESTS: 'command.set_property_value_requests',
    SET_PROPERTY_VALUE_REQUEST_SUMMARIES: 'setPropertyValueRequestSummaries',
    SET_PROPERTY_VALUE_RESPONSES: 'command.set_property_value_responses',
    APPLICATION_INVITES: 'applicationInvites',
    PROPERTIES_CHANGED: 'properties_changed'
};

var RequestCallback = {
    add: function(requestId, callback) {
        requestCallbacks[requestId] = callback;
    },
    get: function(requestId) {
        return requestCallbacks[requestId];
    },
    contains: function(requestId) {
        return requestCallbacks.hasOwnProperty(requestId);
    },
    remove: function(requestId) {
        delete requestCallbacks[requestId];
    },
    handleRequestCallback: function(request, oldRequest) {
        console.log('Handling request callback, old status: ' + oldRequest.status + ', new status: ' + request.status);

        if (!RequestCallback.contains(request.id)) {
            console.log('No request callback exist for request with id "' + request.id + '"');
            return;
        }

        if (oldRequest.status === responseStatus.WAITING) {
            if (request.status === responseStatus.SUCCESS && RequestCallback.contains(request.id)) {
                callback = RequestCallback.get(request.id);
                callback(null, request);
                RequestCallback.remove(request.id);
            } else if (request.status === responseStatus.ERROR && RequestCallback.contains(request.id)) {
                callback = RequestCallback.get(request.id);
                callback( /* Throw error */ null, null);
                RequestCallback.remove(request.id);
            }
        }
    }
}

// Helper class for communicating with the Conductor server.
module.exports = {
    Devices: null,
    SetPropertyValueResponses: null,
    SetPropertyValueRequests: null,
    Invites: null,
    Subscription: {
        DEVICES: 'devices',
        SET_PROPERTY_VALUE_REQUESTS: 'command.set_property_value_requests',
        SET_PROPERTY_VALUE_REQUEST_SUMMARIES: 'command.set_property_value_request_summaries',
        APPLICATION_INVITES: 'application.invites',
        PROPERTIES_CHANGED: 'properties_changed'
    },
    connect: function(url) {
        remote = DDP.connect(url);
        Conductor.Devices = new Meteor.Collection(Collection.DEVICES, remote);
        Conductor.SetPropertyValueResponses = new Meteor.Collection(Collection.SET_PROPERTY_VALUE_REQUESTS, remote);
        Conductor.SetPropertyValueRequests = new Meteor.Collection(Collection.SET_PROPERTY_VALUE_RESPONSES, remote);
        Conductor.SetPropertyValueRequestSummaries = new Meteor.Collection(Collection.SET_PROPERTY_VALUE_REQUEST_SUMMARIES, remote);
        Conductor.PropertiesChanged = new Meteor.Collection(Collection.PROPERTIES_CHANGED, remote);
        Conductor.Invites = new Meteor.Collection(Collection.APPLICATION_INVITES, remote);

        Conductor.SetPropertyValueRequestSummaries.find({}).observe({
            changed: function(request, oldRequest) {
                console.log('Request summary changed.');
                RequestCallback.handleRequestCallback(request, oldRequest);
            }
        })
    },
    setApiKey: function(privateApiKey) {
        apiKey = privateApiKey;
    },
    subscribe: function(name, callback) {
        remote.subscribe(name, {
            apiKey: apiKey
        }, callback);
    },
    setPropertyValue: function(filter, propertyValue, callback) {
        var command = {
            filter: filter,
            propertyValue: propertyValue
        };

        remote.call('Command.setPropertyValue', apiKey, command, function(error, result) {
            if (error) {
                callback(error, result);
            } else {
                RequestCallback.add(result.id, callback);
            }
        });
    },
    createInvite: function(id, callback) {
        remote.call('Application.createApplicationInvite', apiKey, id, callback);
    }
}

var lastHeartbeat = new Date();
Meteor.setInterval(function () {
	remote.call("heartbeat", function () {
		lastHeartbeat = new Date();
	});
}, 3000);

var lostConnection = false;
Meteor.setInterval(function () {
	if (hearbeatExpired(lastHeartbeat)) {
		console.warn("Lost connection with Condcutor server. Trying to reconnect...");
		lostConnection = true;
		remote.reconnect();
	} else if (lostConnection) {
		console.log('Connection to Conductor server re-established.');
		lostConnection = false;
	}
}, 5000);

function hearbeatExpired(lastHeartbeat) {
	var expires = 5000; // If we don't get an heartbeat for 5 seconds, then we assume that we are not connected to the server.
	return new Date().getTime() - lastHeartbeat.getTime() > expires;
}
